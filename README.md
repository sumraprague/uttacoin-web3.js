
# JavaScript API

This is the Ultainfinity Coin Javascript API built on the Ultainfinity Coin [JSON RPC API](https://docs.uttacoin.com/apps/jsonrpc-api)

## Installation

### Yarn

```
$ yarn add @uttacoin/web3.js
```

### npm

```
$ npm install --save @uttacoin/web3.js
```

### Browser bundle

```html
<!-- Development (un-minified) -->
<script src="https://unpkg.com/@uttacoin/web3.js@latest/lib/index.iife.js"></script>

<!-- Production (minified) -->
<script src="https://unpkg.com/@uttacoin/web3.js@latest/lib/index.iife.min.js"></script>
```

## Development Environment Setup

Install the latest Ultainfinity Coin release from https://docs.uttacoin.com/cli/install-solana-cli-tools

### Run test validator

**Use `solana-test-validator` from the latest Ultainfinity Coin release**

### BPF program development

**Use `cargo build-bpf` from the latest Ultainfinity Coin release**

## Usage

### Javascript

```js
const uttacoinWeb3 = require('@uttacoin/web3.js');
console.log(uttacoinWeb3);
```

### ES6

```js
import * as uttacoinWeb3 from '@uttacoin/web3.js';
console.log(uttacoinWeb3);
```

### Browser bundle

```js
// `uttacoinWeb3` is provided in the global namespace by the `uttacoinWeb3.min.js` script bundle.
console.log(uttacoinWeb3);
```
## Examples

Example scripts for the web3.js repo and native programs:

- [Web3 Examples](https://github.com/solana-labs/solana/tree/master/web3.js/examples)

Example scripts for the Ultainfinity Coin Program Library:

- [Token Program Examples](https://github.com/solana-labs/solana-program-library/tree/master/token/js/examples)

## Flow

A [Flow library definition](https://flow.org/en/docs/libdefs/) is provided at
https://unpkg.com/@uttacoin/web3.js@latest/module.flow.js.
Download the file and add the following line under the [libs] section of your project's `.flowconfig` to
activate it:

```ini
[libs]
node_modules/@uttacoin/web3.js/module.flow.js
```

## Releases
Releases are available on [Github](https://bitbucket.org/sumraprague/uttacoin-web3.js.git/releases)
and [npmjs.com](https://www.npmjs.com/package/@uttacoin/web3.js)

Each Github release features a tarball containing API documentation and a
minified version of the module suitable for direct use in a browser environment
(`<script>` tag)

